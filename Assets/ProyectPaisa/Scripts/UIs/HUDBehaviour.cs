﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

[RequireComponent(typeof(TimerArcade))]
public class HUDBehaviour : UIBase
{
    private TimerArcade timerArcade;
    [SerializeField] private TextMeshPro time_Txt;
 

    protected override void Awake()
    {
        base.Awake();
        view = UIView.HUD;
    }

    
    public override void Initialize(Camera camera)
    {
        base.Initialize(camera);
        timerArcade = GetComponent<TimerArcade>();
        timerArcade.Initialise(time_Txt);
        timerArcade.StartTimer();
    }



}
