﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static Action<UIView, bool> ChangeView;

    private UIBase[] primaryViews;
    private Camera uiCamera;
    private UIBase actualUI;
    [SerializeField] private HUDBehaviour hudBehaviour;
    [SerializeField] private GameObject victoryUI;
    [SerializeField] private GameObject defeatUI;

    private void Awake()
    {
        ChangeView += OnChangeView;
        victoryUI.SetActive(false);
        defeatUI.SetActive(false);
    }

    private void OnDisable()
    {
        ChangeView -= OnChangeView;
    }

    public void Initialize()
    {
        uiCamera = GetComponentInChildren<Camera>();
        primaryViews = GetComponentsInChildren<UIBase>();
        DefaultViewsValues();
    }

    private void DefaultViewsValues()
    {
        for (int i = 0; i < primaryViews.Length; i++)
        {
            primaryViews[i].CloseView(true);
        }  
    }

    private void OnChangeView(UIView view, bool isPrimary)
    {
        if(isPrimary)
        {
            actualUI = SearchView(view);
            actualUI.gameObject.SetActive(true);
            SelectPrimaryView(actualUI, view);
        }
        else
        {
            if (view == UIView.QuestGameOver)
            {
                defeatUI.SetActive(true);
            }
            else if (view == UIView.QuestGameOverWin)
            {
                victoryUI.SetActive(true);
            }
            //FactoryView.CreateView(view, transform, uiCamera);
        }
        
    }

    private void SelectPrimaryView(UIBase actualView, UIView selectedView)
    {
        switch (selectedView)
        {
            case UIView.Main:
                actualView.GetComponent<MainBehaviour>().Initialize(uiCamera);
                break;
            case UIView.HUD:
                actualView.GetComponent<HUDBehaviour>().Initialize(uiCamera);
                break;
            default:
                break;
        }
    }
    private UIBase SearchView(UIView _view)
    {
        UIBase view = null;
        for (int i = 0; i < primaryViews.Length; i++)
        {
            if(_view == primaryViews[i].View)
            {
                view = primaryViews[i];
                break;
            }
        }
        
        return view;
    }
}
