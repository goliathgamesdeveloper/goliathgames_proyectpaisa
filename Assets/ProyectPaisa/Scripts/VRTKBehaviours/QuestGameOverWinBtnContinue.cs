﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Examples;
using VRTK.Controllables;

public class QuestGameOverWinBtnContinue : ControllableReactor
{
    protected override void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        base.MaxLimitReached(sender, e);
        MisionController.CompleteQuest(true);
        transform.parent.parent.gameObject.SetActive(false);
    }
}
