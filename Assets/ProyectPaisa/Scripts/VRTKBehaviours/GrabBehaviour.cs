﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class GrabBehaviour : VRTK_InteractableObject
{
    private GameObject itemDescription;
    public bool isItemUsedInQuest = false;
    
    protected override void Awake()
    {
        if(GetComponentInChildren<Canvas>() != null)
        {
            itemDescription = GetComponentInChildren<Canvas>().gameObject;
            itemDescription.SetActive(false);
        }   
        
    }

    public override void OnInteractableObjectGrabbed(InteractableObjectEventArgs e)
    {
        base.OnInteractableObjectGrabbed(e);
        if (itemDescription != null)
            itemDescription.SetActive(true);
    }

    public override void OnInteractableObjectUngrabbed(InteractableObjectEventArgs e)
    {
        base.OnInteractableObjectUngrabbed(e);
        if (itemDescription != null)
            itemDescription.SetActive(false);
    }
}
