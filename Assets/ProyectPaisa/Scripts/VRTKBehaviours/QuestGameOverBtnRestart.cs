﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK.Examples;
using VRTK.Controllables;

public class QuestGameOverBtnRestart : ControllableReactor
{
    protected override void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        base.MaxLimitReached(sender, e);
        MisionController.RestartQuest();
        transform.parent.parent.gameObject.SetActive(false);
    }
}
