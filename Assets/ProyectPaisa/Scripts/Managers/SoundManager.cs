using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Singlenton Administrador del Sonido
/// </summary>
public class SoundManager : MonoBehaviour
{
	public static Action<string, bool> PlaySound;
	public static Action<string[]> PlaySoundStack;

	private AudioSource[] sources;
	private int soundStackPosition = 0;
	private string[] soundStack;

	private void Awake() 
	{
		sources = GetComponentsInChildren<AudioSource>();
		PlaySound += OnPlaySoundByName;
		PlaySoundStack += OnPlaySoundStack;
	}
	

	private void OnDisable()
	{
		PlaySound -= OnPlaySoundByName;
		PlaySoundStack -= OnPlaySoundStack;
	}


	private void LoadState()
	{
		if (PlayerPrefs.HasKey ("IsSoundActive")) {
			int isAudioActive = PlayerPrefs.GetInt("IsSoundActive");
			if(isAudioActive == 1)
			{
				GlobalObjects.IsAudioActive = true;
			}
			else
			{
				GlobalObjects.IsAudioActive = false;
			}
		} else {
			if(GlobalObjects.IsAudioActive)
			{
				PlayerPrefs.SetInt("IsSoundActive",1);
				
			}else
			{
				PlayerPrefs.SetInt("IsSoundActive",0);
			}
			PlayerPrefs.Save();
		}
	}

	private void OnPlaySoundByName(string name, bool isLoop = false)
	{
		if (GlobalObjects.IsAudioActive) {

            if (name == Dialogues.Audio_Victory.ToString() ||
                name == Dialogues.Audio_Defeat.ToString() ||
                name == Dialogues.Audio_Quest1_1_Nota3.ToString())
            {
                StopAll();
            }

            for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					if (!sources [i].isPlaying) {
						sources [i].loop = isLoop;
						sources [i].Play ();
					}
					else
					{
						sources[i].Stop();
						sources[i].loop = isLoop;
						sources[i].Play();
					}
				}
			}	
		}
	}

	private void OnPlaySoundStack(string[] soundStack)
	{
		this.soundStack = soundStack;
		PlayStack();
	}

	private void PlayStack()
	{
		if (soundStackPosition <= soundStack.Length - 1)
		{	
			StartCoroutine(StartSoundStack(GetSoundByName(soundStack[soundStackPosition])));
		}
			
	}


	private IEnumerator StartSoundStack(AudioSource sound)
	{
		if (!sound.isPlaying)
		{
			sound.Play();
		}
		else
		{
			sound.Stop();
			sound.Play();
		}

		while (sound.isPlaying)
		{
			yield return new WaitForSeconds(0.1f);
		}
		if (!sound.isPlaying)
		{
			yield return new WaitForSeconds(0.5f);
			if (soundStackPosition <= soundStack.Length -1)
			{
				soundStackPosition += 1;
				PlayStack();
			}
		}	
	}

	private AudioSource GetSoundByName(string name)
	{
		for (int i = 0; i < sources.Length; i++) {
			if (sources [i].gameObject.name == name) {
				return sources[i];
			}
		}
		return null;	
	}


	private void PlayResourcesSoundByName(string name, string address, bool isLoop = false)
	{

		if (GlobalObjects.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {

					AudioClip newAudio = Resources.Load <AudioClip> (address);
					sources [i].clip = newAudio;
					sources [i].loop = isLoop;
					sources [i].Play ();

				}
			}
		}
	}

	private float GetAudioLength(string name)
	{
		if (GlobalObjects.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					return sources[i].clip.length;
				}
			}
		}
		return 0;
	}


	private void PauseSoundByName(string name)
	{

		
		if (GlobalObjects.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					sources [i].Pause ();
				}
			}
		}
	}

	private void StopSoundByName(string name, bool isLoop)
	{

		if (GlobalObjects.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					sources [i].loop = isLoop;
					sources [i].Stop ();
				}
			}
		}
	}

	private void ChangeSoundSpeedByName(string name, float speed)
	{

		if (GlobalObjects.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
						sources [i].pitch = speed;
				}
			}
		}
	}

	private void ActiveAll()
	{

		GlobalObjects.IsAudioActive = true;
		for (int i = 0; i < sources.Length; i++) {
			sources[i].enabled = true;
		}
		PlayerPrefs.SetInt("IsSoundActive",1);
		PlayerPrefs.Save();
	}

	private void DesactiveAll()
	{

		GlobalObjects.IsAudioActive = false;
		for (int i = 0; i < sources.Length; i++) {
			sources[i].enabled = false;
		}
		PlayerPrefs.SetInt("IsSoundActive",0);
		PlayerPrefs.Save();
	}

	private void SilenceAll()
	{

		if (GlobalObjects.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				sources [i].volume = 0;
			}
		}
	}

	private void StopAll()
	{

		if (GlobalObjects.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				sources [i].loop = false;
				sources [i].Stop ();
			}
		}
	}

	private float GetSoundLengthByName(string name)
	{

		if (GlobalObjects.IsAudioActive) {
			for (int i = 0; i < sources.Length; i++) {
				if (sources [i].gameObject.name == name) {
					return	sources [i].clip.length;
				}
			}
		}

		return 0;
	}

}
