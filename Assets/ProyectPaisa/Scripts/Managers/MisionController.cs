﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class MisionController : MonoBehaviour
{
    public static Action<bool> CompleteQuest;
    public static Action RestartQuest;
    public static Action CompleteAllQuestActivity;
    

    [SerializeField] private Quest1Behaviour quest1Behaviour;
    [SerializeField] private Quest1_Arrieros1Behaviour quest1_Arrieros1Behaviour;
    [SerializeField] private Quests quest;


    private void Awake()
    {
        CompleteQuest += OnCompleteQuest;
        CompleteAllQuestActivity += OnCompleteAllQuestActivity;
        RestartQuest += OnRestartQuestActivity;
    }

    private void OnDisable()
    {
        CompleteQuest -= OnCompleteQuest;
        CompleteAllQuestActivity -= OnCompleteAllQuestActivity;
        RestartQuest -= OnRestartQuestActivity;
    }

    private void OnCompleteQuest(bool isSuccess)
    {
        if (isSuccess)
        {
            switch (quest)
            {
                case Quests.Quest0_Inicio:
                    ChangeQuest(Quests.Quest1_Arrieros);
                    break;

                case Quests.Quest1_Arrieros:
                    ChangeQuest(Quests.Quest1_Arrieros1);
                    break;

                case Quests.Quest1_Arrieros1:
                    ChangeQuest(Quests.Credits);
                    break;

                default:
                    break;
            }
        }
        else
        {
            UIManager.ChangeView(UIView.QuestGameOver, false);
            switch (quest)
            {
                case Quests.Quest0_Inicio:
                    break;

                case Quests.Quest1_Arrieros:
                    quest1Behaviour.DoGameOverBehaviour();
                    break;

                case Quests.Quest1_Arrieros1:
                    quest1_Arrieros1Behaviour.DoGameOverBehaviour();
                    break;

                default:
                    break;
            } 
        }

    }

    

    private void ChangeQuest(Quests quest)
    {
        switch (quest)
        {
            case Quests.Quest0_Inicio:
                break;

            case Quests.Quest1_Arrieros:
                SceneManager.LoadScene(Scenes.Quest1_Arrieros.ToString());
                break;

            case Quests.Quest1_Arrieros1:
                SceneManager.LoadScene(Scenes.Quest1_Arrieros1.ToString());
                break;

            case Quests.Credits:
                SceneManager.LoadScene(Scenes.Credits.ToString());
                break;
            default:
                break;
        }
    }

    private void OnCompleteAllQuestActivity()
    {
        switch (quest)
        {
            case Quests.Quest0_Inicio:
                break;

            case Quests.Quest1_Arrieros:
                quest1Behaviour.StopAllActivitys();
                quest1Behaviour.DoGameOverWinBehaviour();
                break;

            case Quests.Quest1_Arrieros1:
                quest1_Arrieros1Behaviour.StopAllActivitys();
                quest1_Arrieros1Behaviour.DoGameOverWinBehaviour();
                break;

            default:
                break;
        }
    }

    private void OnRestartQuestActivity()
    {
        switch (quest)
        {
            case Quests.Quest0_Inicio:
                break;

            case Quests.Quest1_Arrieros:
                quest1Behaviour.ResetActivity();
                break;

            case Quests.Quest1_Arrieros1:
                quest1_Arrieros1Behaviour.ResetActivity();
                break;

            default:
                break;
        }
    }
}
