﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    [SerializeField] private UIManager uiManager;
    
    void Start()
    {
        uiManager.Initialize();
        GameState(GameStates.Gameplay);
    }

    private void GameState(GameStates gameState)
    {
        switch (gameState)
        {
            case GameStates.Tutorial:
                break;

            case GameStates.Gameplay:
                if (UIManager.ChangeView != null)
                {
                    UIManager.ChangeView(UIView.HUD, true);
                }
                break;

            default:
                break;
        }
    }
}
