﻿public enum Quests
{
    Quest0_Inicio = 0,
    Quest1_Arrieros = 1,
    Quest1_Arrieros1 = 2,
    Credits = 3
}

public enum Scenes
{
    Quest0_Inicio = 0,
    Quest1_Arrieros = 1,
    Quest1_Arrieros1 = 2,
    Credits = 3,
    Main = 4
}

public enum UIView
{
    None = -1,
    Main = 0,
    HUD = 1,
    Settings = 2,
    Loading = 3,
    QuestGameOver = 4,
    QuestGameOverWin = 5
}

public enum GameStates
{
    Gameplay = 0,
    Pause = 1,
    Tutorial = 2,
    GameOver = 3,
    Main = 4
}

public enum ArrieroAnimations
{
    Salute = 0,
    Victory = 1,
    Defeat = 2
}

public enum Quest1_Arrieros1_CharactersAnimation
{
    Run = 0,
    No = 1
}

public enum Quest1_Arreieros1_ItemType
{
    Wood = 0,
    Gold = 1,
    Coffe = 2,
    Cloth = 3
    
}

public enum Dialogues
{
    Audio_Creditos = 0,
    Audio_Defeat = 1,
    Audio_Quest0_Nota1 = 2,
    Audio_Quest0_Nota2 = 3,
    Audio_Quest0_Nota3 = 4,
    Audio_Quest1_1_Nota2 = 5,
    Audio_Quest1_1_Nota3  = 6,
    Audio_Quest1_Nota1 = 7,
    Audio_Victory = 8
}

public enum SFX
{
    SFX_Defeat = 0,
    SFX_Victory = 1
}