﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1Behaviour : MonoBehaviour
{
    private Vector3[] initItemsPositions;
    private Vector3[] initItemsRotation;
    [SerializeField] private Transform[] items;
    [SerializeField] private TimerArcade timerArcade;
    [SerializeField] private GameObject mule;
    [SerializeField] private ArrieroBehaviour arriero;
    private Vector3 muleStartPosition;
    private int muleWalkDuration = 10;
    private float muleCurrentDuration = 0;
    private float muleWalkDelay = 0.1f;

    private void Awake()
    {
        initItemsPositions = new Vector3[items.Length];
        initItemsRotation = new Vector3[items.Length];
        muleStartPosition = mule.transform.position;
        for (int i = 0; i < items.Length; i++)
        {
            initItemsPositions[i] = items[i].transform.position;
            initItemsRotation[i] = items[i].transform.eulerAngles;
        }
    }

    private void Start()
    {
        SoundManager.PlaySound(Dialogues.Audio_Quest1_Nota1.ToString(), false);
    }

    public void StopAllActivitys()
    {
        timerArcade.StopAllCoroutines();
    }

    public void ResetActivity()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].transform.position = initItemsPositions[i];
            Quaternion itemRotation = Quaternion.Euler(initItemsRotation[i]);
            items[i].transform.rotation = itemRotation;
        }
        StopCoroutine(DoMuleWalkOverTime());
        mule.transform.position = muleStartPosition;
        mule.GetComponentInChildren<MuleBoxItemsSensor>().ResetCountItems();
        ResetItemsData();
        muleCurrentDuration = 0;
        timerArcade.StartTimer();
        arriero.DoAnimation(ArrieroAnimations.Salute, true);
        arriero.DoAnimation(ArrieroAnimations.Defeat, false);
        arriero.DoAnimation(ArrieroAnimations.Victory, false);
    }

    public void DoGameOverBehaviour()
    {
        SoundManager.PlaySound(Dialogues.Audio_Defeat.ToString(), false);
        SoundManager.PlaySound(SFX.SFX_Defeat.ToString(), false);
        arriero.DoAnimation(ArrieroAnimations.Defeat, true);
        StartCoroutine(DoMuleWalkOverTime());
    }

    public void DoGameOverWinBehaviour()
    {
        SoundManager.PlaySound(Dialogues.Audio_Victory.ToString(), false);
        SoundManager.PlaySound(SFX.SFX_Victory.ToString(), false);
        arriero.DoAnimation(ArrieroAnimations.Victory, true);
    }

    private void ResetItemsData()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].GetComponent<GrabBehaviour>().isItemUsedInQuest = false;
        }
    }

    private IEnumerator DoMuleWalkOverTime()
    {
        while (muleCurrentDuration <= muleWalkDuration)
        {
            mule.transform.Translate(Vector3.forward * muleWalkDelay, Space.Self);
            muleCurrentDuration += muleWalkDelay;
            yield return new WaitForSeconds(muleWalkDelay);
        }
        StopCoroutine(DoMuleWalkOverTime());
    }
}
