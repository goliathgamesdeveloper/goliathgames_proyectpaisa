﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuleBoxItemsSensor : MonoBehaviour
{
    private int currentItems = 0;
    private int totalItems = 2;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Quest1_Items")
        {
            if (!collider.gameObject.GetComponent<GrabBehaviour>().isItemUsedInQuest)
            {
                currentItems += 1;
                collider.gameObject.GetComponent<GrabBehaviour>().isItemUsedInQuest = true;
            }
            CheckIfIsComplete();
        }
    }

    public void ResetCountItems()
    {
        currentItems = 0;
    }

    private bool CheckIfIsComplete()
    {
        if (currentItems == totalItems)
        {
            MisionController.CompleteAllQuestActivity();
            UIManager.ChangeView(UIView.QuestGameOverWin, false);
            return true;
        }
        return false;
    }
}
