﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1_Arrieros1_ItemType : MonoBehaviour
{
    [SerializeField] private Quest1_Arreieros1_ItemType itemTypeSelected;
    public Quest1_Arreieros1_ItemType ItemTypeSelected
    {
        get { return itemTypeSelected; }
    }
}
