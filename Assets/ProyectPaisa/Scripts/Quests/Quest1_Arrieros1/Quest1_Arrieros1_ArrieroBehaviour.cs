﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1_Arrieros1_ArrieroBehaviour : MonoBehaviour
{
    private int arrieroWalkDuration = 10;
    private float arrieroCurrentDuration = 0;
    private float arrieroWalkDelay = 0.1f;
    private Canvas itemDescription;
    private Animator animator;

    private void Awake()
    {
        itemDescription = GetComponentInChildren<Canvas>();
        animator = GetComponent<Animator>();
    }

    public void GoToHome()
    {
        StartCoroutine(DoArrieroWalkOverTime());
        itemDescription.gameObject.SetActive(false);
    }

    public void DoAnimation(Quest1_Arrieros1_CharactersAnimation animation, bool state)
    {
        animator.SetBool(animation.ToString(), state);
    }

    private IEnumerator DoArrieroWalkOverTime()
    {
        transform.Rotate(Vector3.up * 180, Space.Self);
        while (arrieroCurrentDuration <= arrieroWalkDuration)
        {
            transform.Translate(Vector3.forward * arrieroWalkDelay, Space.Self);
            arrieroCurrentDuration += arrieroWalkDelay;
            yield return new WaitForSeconds(arrieroWalkDelay);
        }
        StopCoroutine(DoArrieroWalkOverTime());
    }
}
