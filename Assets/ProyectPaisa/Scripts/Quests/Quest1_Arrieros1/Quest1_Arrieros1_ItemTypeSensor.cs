﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1_Arrieros1_ItemTypeSensor : MonoBehaviour
{
    [SerializeField] private Quest1_Arreieros1_ItemType itemTypeSelected;
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Quest1_Items")
        {
            Quest1_Arreieros1_ItemType itemType = collision.gameObject.GetComponent<Quest1_Arrieros1_ItemType>().ItemTypeSelected;
            if (itemType == itemTypeSelected)
            {
                Quest1_Arrieros1Behaviour.ItemInContainer(itemType, true);
            } else
            {
                Quest1_Arrieros1Behaviour.ItemInContainer(itemTypeSelected, false);
            }          
        }
    }

}
